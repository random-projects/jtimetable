/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.readers;
import  jtimetable2.dataobjects.*;
import  java.io.IOException;
import  java.util.Map;
import  javax.swing.JPanel;

/**
 *
 * @author webpigeon
 */
public interface TimetableReader {
    public void setFilename(String filename) throws IOException;
    public String getFilename();

    public Timetable           getTimetable();
    public Map<String,Module>  getModules();
    public Module              getModule(String moduleCode);
    public Week                getDate(int weekno);

    //public JPanel              getSettingsPane();
}
