/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.readers;
import jtimetable2.settings.SettingController;
import jtimetable2.dataobjects.*;
import jtimetable2.settings.SettingTab;
import java.util.Map;

/**
 *
 * @author webpigeon
 */
public interface EstablishmentController {

    //File related
    public void setFilename(String filename) throws Exception;
    public String getFilename();

    //settings related
    public void setSettings(SettingController sc);
    public SettingController getSettings();

    //timetable reader
    public void setTimetableReader(TimetableReader tr);
    public TimetableReader getTimetableReader();

    //timetable reader releated
    public Timetable getTimetable();
    public Lesson[] getLessons();
    public Map<String, Module> getModules();
    public Week[] getWeeks();

    //Settings Related
    public SettingTab getSettingTab();
}
