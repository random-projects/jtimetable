/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.readers;
import java.util.Map;
import jtimetable2.dataobjects.Lesson;
import jtimetable2.dataobjects.Module;
import jtimetable2.dataobjects.Timetable;
import jtimetable2.dataobjects.Week;
import jtimetable2.settings.SettingController;
import jtimetable2.settings.SettingTab;
import jtimetable2.settings.EssexTimetableTab;

/**
 *
 * @author webpigeon
 */
public class EssexController implements EstablishmentController {
    private TimetableReader tr;
    private SettingController settings;
    private SettingTab st;

    public EssexController(){
        tr = new EssexTimetableReader();
    }

    public void setFilename(String filename) throws Exception {
        this.tr.setFilename(filename);
    }

    public String getFilename() {
        return tr.getFilename();
    }

    public void setSettings(SettingController sc) {
        this.settings = sc;
    }

    public SettingController getSettings() {
        return this.settings;
    }

    public void setTimetableReader(TimetableReader tr) {
        this.tr = tr;
    }

    public TimetableReader getTimetableReader() {
        return tr;
    }

    public Timetable getTimetable() {
        return tr.getTimetable();
    }

    public Lesson[] getLessons() {
        return null;
    }

    public Map<String, Module> getModules() {
        return this.tr.getModules();
    }

    public Week[] getWeeks() {
        return this.getWeeks();
    }

    public SettingTab getSettingTab() {
        if(st == null)
            st = new EssexTimetableTab(this.settings, this);
        return st;
    }
}
