/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.readers;
import jtimetable2.readers.TimetableReader;
import jtimetable2.dataobjects.*;
import java.util.*;
import java.io.*;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;

//Java Excel reader imports
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;

/**
 *
 * @author webpigeon
 */
public class EssexTimetableReader implements TimetableReader {
    private String    filename;
    private Timetable timetable;
    private SortedMap<String, Module> modules;
    private Date[] weekLookup;
    private String[] days = new String[]{
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"
    };

    public EssexTimetableReader(){
        modules = new TreeMap<String, Module>();
    }

    public void setFilename(String filename) throws java.io.IOException {
        this.filename = filename;
        Calendar wee = new GregorianCalendar();
        wee.set(2009, Calendar.OCTOBER, 5, 0, 0, 0);
        wee.setTimeZone(new SimpleTimeZone(0, "GMT"));
        this.importTimetable(wee.getTime());
    }

    public String getFilename() {
        return this.filename;
    }

    public void importTimetable(Date yearStart) throws IOException{
        ArrayList<Lesson> l = new ArrayList<Lesson>();
        modules.clear();

        //create the workbook object and get the first sheet
        Workbook wb = new HSSFWorkbook(new FileInputStream(this.filename));
        Sheet sheet = wb.getSheetAt(0);

        SimpleDateFormat f = new SimpleDateFormat("HH:mm");
        Calendar c = new GregorianCalendar();
        c.setTime(yearStart);

        this.weekLookup = new Date[52];
        for(int w=0; w<52; w++){
            c.add(Calendar.WEEK_OF_YEAR, 1);
            weekLookup[w] = c.getTime();
        }

        for(Row r : sheet){
            try{
                Lesson tmp = new Lesson(null);
                Date startTime = f.parse(r.getCell(5).getStringCellValue(), new ParsePosition(0));
                Long startDay  = getDayValue(r.getCell(4).getStringCellValue(), startTime);
                Integer[] weeks = parseWeeks(r.getCell(7).getStringCellValue());

                tmp.setModule(getModule(r.getCell(0).getStringCellValue()));
                tmp.setGroup(r.getCell(1, Row.CREATE_NULL_AS_BLANK).getStringCellValue());
                tmp.setType(r.getCell(2).getStringCellValue());
                tmp.setStart(parseStartDates(weeks, startDay));
                tmp.setLocation(r.getCell(9).getStringCellValue());
                tmp.setDuration(f.parse(r.getCell(6).getStringCellValue()).getTime() - startTime.getTime());
                l.add(tmp);
            }catch(Exception e){
                System.err.println("unable to import row "+r.getRowNum());
            }
        }

        timetable = new Timetable(l);
    }

    private Date[] parseStartDates(Integer[] weeks, long startTime){
        Date[] da = new Date[weeks.length];
        for(int weekno=0; weekno<weeks.length; weekno++){
            da[weekno] = new Date(weekLookup[weekno].getTime() + startTime);
        }

        return da;
    }

    private long getDayValue(String day, Date startDate){
        long dayval=0;

        for(int i=0; i<days.length; i++){
            if(days[i].startsWith(day)){
                dayval = i*86400000; //day * milies in day
                break;
            }
        }

        return dayval+startDate.getTime();
    }

    /**
     * Because Essex uni is retarded.
     * @param week
     * @return
     */
    private Integer[] parseWeeks(String week){
        Collection<Integer> weeks = new Vector<Integer>();
        String[] cWeek = week.split(",");

        for(String w : cWeek){
            if(w.contains("-")){
                String[] s = w.split("-", 2);
                int start = Integer.parseInt(s[0]);
                int end = Integer.parseInt(s[1]);
                while(start<=end){
                    weeks.add(start);
                    start++;
                }
            }else{
                weeks.add(Integer.parseInt(w));
            }
        }

        return weeks.toArray(new Integer[0]);
    }

    public Module getModule(String code){
        Module m = modules.get(code);
        if(m == null){
            m = new Module(code);
            modules.put(code, m);
        }

        return m;
    }

    public Timetable getTimetable(){
        return this.timetable;
    }

    public Week getDate(int id){
        return null;
    }

    public Map<String, Module> getModules() {
        return this.modules;
    }
}
