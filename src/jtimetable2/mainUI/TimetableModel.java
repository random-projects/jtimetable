/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.mainUI;
import javax.swing.table.AbstractTableModel;
import jtimetable2.dataobjects.*;
import java.util.Map;
import java.util.List;

/**
 *
 * @author webpigeon
 */
public class TimetableModel extends AbstractTableModel {
    private Timetable t;
    private Map<String, List<String>> ma;
    private String[] columns = new String[]{
        "date/time",
        "type",
        "module",
        "room"
    };

    public TimetableModel(Timetable t, Map<String, List<String>> modules){
        this.t = t;
        this.ma = modules;
    }

    public void setModules(Map<String, List<String>> modules){
        this.ma = modules;
    }

    public int getRowCount() {
        return t.lessonWeekCount();
    }

    public int getColumnCount() {
        return this.columns.length;
    }

    @Override
    public String getColumnName(int x){
        return columns[x];
    }

    public void filterLessons(Week w, Map<String, List<String>> modules){
        this.ma = modules;
        this.filterLessons(w);
    }

    public void filterLessons(Week w){
        this.t.filterLessons(w.getStart(), w.getEnd(), ma);
        this.fireTableDataChanged();
    }

    public Object getValueAt(int y, int x) {
        Lesson l = t.getWeekLesson(y);

        switch(x){
            case 0:
                return l.getStartTime()+" for " + l.getDuration()/3600000.0 +" hrs";
            case 1:
                return l.getType();
            case 2:
                return l.getModule()+" "+l.getGroups();
            case 3:
                return l.getLocation();
            default:
                System.err.println("Error - non-existant column requested "+x);
                return null;
        }
    }

}
