/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.mainUI;
import jtimetable2.settings.SettingController;
import jtimetable2.settings.ApplyListener;
import jtimetable2.dataobjects.*;
import java.util.Date;

/**
 *
 * @author webpigeon
 */
public class MainController implements ApplyListener {
    private SettingController settings;
    private MainView  view;
    private TimetableModel model;
    private Week week;

    public MainController(Timetable t, SettingController settings){
        this.settings = settings;
        model = new TimetableModel(t, settings.getMyModules());
        setWeek(new Week(1, new Date(0), new Date(2010, 12, 25)));
        model.filterLessons(getWeek());
        view  = new MainView(this, model);
        view.setVisible(true);
    }

    public void showUI(){
        this.view.setVisible(true);
    }

    public void showSettings(){
        this.settings.showUI();
    }

    public void setTimetable(Timetable t){
        this.model = new TimetableModel(t, settings.getMyModules());
        view.setTimetable(this.model);
    }

    public void setWeek(Week w){
        this.week = w;
    }

    public Week getWeek(){
        return this.week;
    }

    public void fireApplyListener() {
        this.model.filterLessons(getWeek());
    }
}
