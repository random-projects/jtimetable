/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2;
import jtimetable2.readers.EstablishmentController;
import jtimetable2.settings.SettingController;
import jtimetable2.mainUI.MainController;
import javax.swing.UIManager;

/**
 *
 * @author webpigeon
 */
public class Main {
    private static MainController main;
    private static SettingController settings;
    private static EstablishmentController ec;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        //set to native look and feel
        UIManager.setLookAndFeel(
            UIManager.getSystemLookAndFeelClassName());

        //create the settings controller
        settings = new SettingController();
        settings.setPlaces(new PrettyLabel[]{
            new PrettyLabel("University of Essex", "Essex"),
            new PrettyLabel("grrrrr", "shoes"),
        });
        main = new MainController(ec.getTimetable(), settings);
        settings.registerApplyListener(main);

        //create the timetable object
        try{
           main.showUI();
        }catch(Exception e){
           e.printStackTrace();
           if(ec != null)
            ec.getSettingTab();
           settings.showUI();
        }
    }

    public static void readTimetable(String cn) throws Exception{
        System.out.println("CN "+cn+"-> PREV "+ec);
        if(ec != null){
            settings.removeTab(ec.getSettingTab());
            ec = null;
        }

        Class c = Class.forName("jtimetable2.readers."+cn+"Controller");
        ec = (EstablishmentController)c.newInstance();
        ec.setSettings(settings);
        ec.setFilename(settings.getPrefs("Readers").node(cn).get("Timetable", ""));
        if(main != null)
            main.setTimetable(ec.getTimetable());
        ec.getSettingTab();
    }

}
