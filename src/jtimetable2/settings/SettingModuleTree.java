/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.settings;
import java.util.Map;
import java.util.List;
import javax.swing.tree.*;

/**
 *
 * @author webpigeon
 */
public class SettingModuleTree extends javax.swing.tree.DefaultTreeModel{
    private DefaultMutableTreeNode myRoot;
    private Map<String, List<String>> modules;

    public SettingModuleTree(Map<String, List<String>> myModules){
        super(new DefaultMutableTreeNode("Banana", true));
        this.myRoot = new DefaultMutableTreeNode("My Modules", true);
        this.modules = myModules;
        this.refreshTree();
    }

    public void refreshTree(){
        this.myRoot.removeAllChildren();
        for(String module : modules.keySet()){
            DefaultMutableTreeNode mtn = new DefaultMutableTreeNode(module, true);
            for(String group : modules.get(module)){
                mtn.add(new DefaultMutableTreeNode(group, false));
            }
            this.myRoot.add(mtn);
        }
        super.setRoot(myRoot);
        this.reload();
    }

}
