/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.settings;
import  javax.swing.ComboBoxModel;
import  javax.swing.event.ListDataListener;
import  java.util.List;
import  java.util.ArrayList;
import  java.util.Collection;
import  jtimetable2.dataobjects.Module;

/**
 *
 * @author webpigeon
 */
public class SettingModelBox implements ComboBoxModel{
    private List<Module> items;
    private int selected;
    private ListDataListener listener;

    public SettingModelBox(Collection<Module> ml){
        this.items = new ArrayList<Module>(ml);
    }
    

    public void setSelectedItem(Object anItem) {
        Module item = (Module)anItem;
        for(int i=0; i<items.size(); i++){
            if(items.get(i).equals(item)){
                selected = i;
            }
        }
    }

    public Object getSelectedItem() {
        return items.get(selected);
    }

    public int getSize() {
        return items.size();
    }

    public Object getElementAt(int index) {
        return items.get(index);
    }

    public void addListDataListener(ListDataListener l) {
        this.listener = l;
    }

    public void removeListDataListener(ListDataListener l) {
        this.listener = null;
    }

}
