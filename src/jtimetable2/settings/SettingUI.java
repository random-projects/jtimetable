/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jtimetable2.settings;

import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.Component;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;

/**
 *
 * @author webpigeon
 */
public class SettingUI extends JDialog implements ActionListener {

    private SettingController controller;
    //private PrefsModel model;
    //private ArrayList<SettingTab> tabs;
    private JTabbedPane settingTabs;
    private JButton applyButton;
    private JButton cancelButton;

    public SettingUI(JFrame parent, SettingController controller) {
        super(parent, "Settings", true);
        this.controller = controller;
        this.setPreferredSize(new Dimension(640, 360));
        this.setResizable(false);
        this.initComponents();

        // Set the settings dialoge to open in the centre of the screen
        Dimension dim = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2, dim.height / 2);
    }

    public void initComponents() {
        //set the layout manager
        this.setLayout(new BorderLayout());

        //create the tab box
        this.settingTabs = new JTabbedPane();
        this.add(this.settingTabs, BorderLayout.CENTER);

        JPanel control = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        //setup apply button
        this.applyButton = new JButton("Apply");
        this.applyButton.setActionCommand("apply");
        this.applyButton.addActionListener(this);
        control.add(this.applyButton);

        //setup cancel button
        this.cancelButton = new JButton("Cancel");
        this.cancelButton.setActionCommand("cancel");
        this.cancelButton.addActionListener(this);
        control.add(this.cancelButton);

        //add all the features
        this.add(control, BorderLayout.PAGE_END);

        this.pack();
    }

    public void addTab(String title, SettingTab t){
        this.settingTabs.add(title, t);
    }

    public void setEnabledTab(SettingTab t, boolean state) {
        int index = this.settingTabs.indexOfComponent(t);
        this.settingTabs.setEnabledAt(index, state);
    }

    public void removeTab(SettingTab t) {
        t.prepRemoveTab();
        this.settingTabs.remove(t);
        System.out.println(java.util.Arrays.toString(settingTabs.getComponents()));
    }

    public void reloadSettings() {
        int cnum = 0;
        for (Component c : settingTabs.getComponents()) {
            if (settingTabs.isEnabledAt(cnum) && c instanceof SettingTab) {
                SettingTab s = (SettingTab) c;
                s.reloadSettings();
            }
            cnum++;
        }
    }

    protected void applyBtnPressed() {
        try {
            int cnum = 0;
            for (Component c : settingTabs.getComponents()) {
                if (settingTabs.isEnabledAt(cnum) && c instanceof SettingTab) {
                    SettingTab s = (SettingTab) c;
                    s.onApply();
                }
                cnum++;
            }
            this.setVisible(false);
            this.controller.fireApplyEvent();
        } catch (Exception e) {
            System.err.println("Error while applying settings: " + e);
            e.printStackTrace();
        }
    }

    protected void closeBtnPressed() {
        this.setVisible(false);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("apply")) {
            this.applyBtnPressed();
            return;
        }

        if (e.getActionCommand().equals("cancel")) {
            this.closeBtnPressed();
            return;
        }
    }
}
