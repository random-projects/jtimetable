/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jtimetable2.settings;

/**
 *
 * @author webpigeon
 */
public abstract class SettingTab extends javax.swing.JPanel {
    protected String title;
    protected SettingController controller;

    public SettingTab(SettingController c, String title) {
        super();
        this.title = title;
        this.controller = c;
        this.controller.addTab(title, this);
    }

    public abstract void reloadSettings();
    public abstract void prepRemoveTab();
    public abstract void onApply() throws Exception;

    @Override
    public String toString(){
        return title;
    }
}
