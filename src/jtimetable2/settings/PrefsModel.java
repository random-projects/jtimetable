/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.settings;
import java.util.TreeMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.prefs.*;
import java.io.File;

/**
 *
 * @author webpigeon
 */
public class PrefsModel {
    private Map<String, List<String>> module;
    private Preferences prefs;

    public PrefsModel() throws Exception{
        module = new TreeMap<String, List<String>>();
        prefs = Preferences.userRoot().node("JTimetable");
        loadModules();
    }

    /**
     * Save a filename to the settings
     * 
     * @param type The key to the filename
     * @param x The file to be saved
     */
    public void setFilename(String type, File x){
       prefs.node("FileNames").put(type,x.toString());
    }

    /**
     * Get a file from the prefrences
     * 
     * @param type the key of the filename
     * @return the file assicated with the key
     */
    public File getFilename(String type){
        return new File(prefs.node("FileNames").get(type, ""));
    }

    public Preferences getPrefs(String node){
        return this.prefs.node(node);
    }

    public boolean prefsExist(String node){
        try{
            return this.prefs.nodeExists(node);
        }catch(java.util.prefs.BackingStoreException e){
            System.err.println(e);
            e.printStackTrace();
        }

        return false;
    }

    public void saveModules(){
        try{
        prefs.node("modules").clear();

        for(String m : this.module.keySet()){
            String gc = "";
            for(String g : this.module.get(m)){
                gc = gc + g + ",";
            }
            gc = gc.substring(0, gc.length()-1);
            prefs.node("modules").put(m, gc);
        }
        }catch(java.util.prefs.BackingStoreException e){
            System.err.println("PrefsModel: "+e);
        }
    }

    private void loadModules() throws Exception{
        module.clear();
        for(String k : prefs.node("modules").keys()){
            String[] g = prefs.node("modules").get(k, "").split(",");
            module.put(k, new ArrayList<String>(Arrays.asList(g)));
        }
    }

   public void addGroup(String module, String group){
        List<String> groups = this.module.get(module);
        if(groups==null){
            groups = new ArrayList<String>();
            this.module.put(module, groups);
        }

        if(!groups.contains(group)){
            groups.add(group);
        }

    }

    public void removeModule(String module){
        this.module.remove(module);
    }

    public void removeGroup(String module, String group){
        List<String> groups = this.module.get(module);
        if(groups != null){
            groups.remove(group);
        }
    }

    public Map<String, List<String>> getModules(){
        return module;
    }

}
