/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.settings;
import java.util.Map;
import javax.swing.JFrame;
import java.util.List;
import java.io.File;
import java.util.prefs.Preferences;
import jtimetable2.PrettyLabel;

/**
 *
 * @author webpigeon
 */
public class SettingController {
    private PrefsModel model;
    private SettingUI view;
    private GeneralTab gt;
    private ApplyListener al;

    public SettingController(){
        try {
            model = new PrefsModel();
            view = new SettingUI(new JFrame(), this);

            //load all our tabs
            gt = new GeneralTab(this);
        } catch (Exception e) {
            System.err.println("Setting Error: "+e.toString());
            e.printStackTrace();
        }
    }

    public void setPlaces(PrettyLabel[] pla){
        this.gt.setPlaces(pla);
    }

    public void registerApplyListener(ApplyListener al){
        this.al = al;
    }

    public void fireApplyEvent(){
        if(al != null)
            this.al.fireApplyListener();
    }

    public void addTab(String title, SettingTab t){
        this.view.addTab(title, t);
    }

    public void removeTab(SettingTab t){
        this.view.removeTab(t);
    }

    public void setTabEnabled(SettingTab t, boolean state){
        this.view.setEnabledTab(t, state);
    }

    public PrefsModel getModel(){
        return this.model;
    }

    public void setModel(PrefsModel m){
        this.model = m;
    }

    public Map<String, List<String>> getMyModules(){
        return this.model.getModules();
    }

    public File getFilename(String name){
        return this.model.getFilename(name);
    }

    public void setFilename(String name, File file){
        this.model.setFilename(name, file);
    }

    public Preferences getPrefs(String node){
        return this.model.getPrefs(node);
    }

    public void showUI(){
        this.view.reloadSettings();
        this.view.setVisible(true);
    }

    public void hideUI(){
        this.view.setVisible(false);
    }
}
