/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.dataobjects;
import java.util.*;

/**
 *
 * @author webpigeon
 */
public class Module {
    private String code;
    private SortedSet<String> groups;

    public Module(String code){
        this.code = code;
        this.groups = new TreeSet<String>();
    }

    public void addGroup(String[] groups){
        for(String group : groups){
            this.groups.add(group);
        }
    }

    public String getName(){
        return code;
    }

    public void addGroup(String group){
        this.groups.add(group);
    }

    public Collection<String> getGroups(){
        return this.groups;
    }

    @Override
    public String toString(){
        return this.code;
    }
}
