/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.dataobjects;
import  java.util.ArrayList;
import  java.util.Arrays;
import  java.util.List;
import java.util.Date;
import java.util.Collections;
import java.util.Map;

/**
 *
 * @author webpigeon
 */
public class Timetable {
    private List<Lesson> lessons;
    private List<Lesson> weekLesson;

    public Timetable(Lesson[] lessons){
        this.lessons = new ArrayList<Lesson>(Arrays.asList(lessons));
        this.weekLesson = new ArrayList<Lesson>();
        Collections.sort(this.lessons);
        Collections.sort(this.weekLesson);
    }

    public Timetable(List lessons){
        this.lessons = new ArrayList<Lesson>(lessons);
        this.weekLesson = new ArrayList<Lesson>();
        Collections.sort(this.lessons);
        Collections.sort(this.weekLesson);
    }

    public int lessonCount(){
        return lessons.size();
    }

    public int lessonWeekCount(){
        return weekLesson.size();
    }

    public Lesson getWeekLesson(int i){
        return weekLesson.get(i);
    }

    public List<Lesson> getWeekLessons(){
        return this.weekLesson;
    }

    public Lesson getLesson(int i){
        return lessons.get(i);
    }


    public void filterLessons(Date startDate, Date endDate, Map<String, List<String>> myModules){
        weekLesson.clear();
        System.out.println(myModules);
        for(Lesson l : lessons){
            System.out.println("I have found a lesson");
            if(l.inBounds(startDate, endDate)){
                System.out.println("I have a lesson in this week");
                for(String mn : myModules.keySet()){
                    List<String> ml  = myModules.get(mn);
                    if(l.getModule().getName().equals(mn) && l.hasGroup(ml)){
                        System.out.println("Your taking this module");
                        weekLesson.add(l);
                    }
                }
            }
        }
    }
}
