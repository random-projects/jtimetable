/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jtimetable2.dataobjects;

import java.sql.Time;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author webpigeon
 */
public class Lesson implements Comparable<Lesson> {

    private Module module;
    private String[] groups;
    private String type;
    private String location;
    private Date[] start;
    private Time duration;
    private ArrayList<Week> weeks;
    private SimpleDateFormat f;

    public Lesson(Module m) {
        this.module = m;
        this.f = new SimpleDateFormat("EEE, kk:mm");
    }

    public void setModule(Module m){
        this.module = m;
    }

    public void setLocation(String location){
        this.location = location;
    }

    public void setGroup(String group){
        if(group.contains(",")){
            this.setGroups(group.split(","));
        }else{
            this.setGroups(new String[]{group});
        }
    }

    public void setWeek(Week[] w){
        this.weeks.clear();
        this.weeks.addAll(Arrays.asList(w));
    }

    public void addWeek(Week w){
        this.weeks.add(w);
    }

    public Week[] getWeeks(){
        return this.weeks.toArray(new Week[0]);
    }

    public String getLocation(){
        return location;
    }

    public boolean inWeek(Week w){
        return this.weeks.contains(w);
    }

    public void setGroups(String[] groups){
        this.module.addGroup(groups);
        this.groups = groups;
    }

    public void setType(String type){
        this.type = type;
    }

    public void setStart(Date[] start){
        this.start = start;
    }

    public Module getModule(){
        return module;
    }

    public String getGroups(){
        return Arrays.toString(groups);
    }

    public boolean hasGroup(String chckGroup){
        for(String group : groups){
            if(group.equals(chckGroup) || group.equals("")){
                return true;
            }
        }

        return false;
    }

    public void setDuration(Long time){
        this.duration = new Time(time);
    }

    public long getDuration(){
        return this.duration.getTime();
    }

    public boolean hasGroup(Collection<String> chkGroups){
        for(String group : chkGroups){
            if(hasGroup(group)){
                return true;
            }
        }

        return false;
    }

    public String getType(){
        return type;
    }

    public String getStartTime(){
        return  f.format(start[0]);
    }

    public Date[] getStartDate(){
        return this.start;
    }

    public boolean inBounds(Date start, Date end){
        for(Date s : this.start){
            if(s.after(start) || s.equals(start)){
                if(s.before(end) || s.equals(end)){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public String toString(){
        return "Lesson for "+this.module+" with group "+Arrays.toString(this.groups);
    }

    public int compareTo(Lesson o) {
        return o.getStartTime().compareTo(this.getStartTime()) * -1;
    }
}
