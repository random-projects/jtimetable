/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2.dataobjects;
import java.util.Date;

/**
 *
 * @author webpigeon
 */
public class Week implements Comparable<Week> {
    private int id;
    private Date start;
    private Date end;

    public Week(int id, Date start, Date end){
        this.id    = id;
        this.start = start;
        this.end   = end;
    }

    public int compareTo(Week w) {
        return this.start.compareTo(w.getStart());
    }
    
    public boolean inWeek(Date d){
        int tot = start.compareTo(d) + end.compareTo(d);
        if(tot == -2 || tot == 2){
            return false;
        }
        
        return true;
    }

    public int getId(){
        return this.id;
    }

    public Date getStart(){
        return this.start;
    }

    public Date getEnd(){
        return this.end;
    }
}
