/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jtimetable2;

/**
 *
 * @author webpigeon
 */
public class PrettyLabel {
    private String pretty;
    private String real;

    public PrettyLabel(String pretty, String real){
        this.pretty = pretty;
        this.real   = real;
    }

    public String getReal(){
        return real;
    }

    @Override
    public String toString(){
        return pretty;
    }

}
